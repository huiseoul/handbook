# TL;DR

- Huiseoul 과 함께할 **신입**/경력 front end engineer 를 찾습니다.
- 주변에 있는 좋은 분들을 소개해 주세요! 입사가 확정되면 **추천하신 분과 추천받으신 분 모두**에게 애플 워치3를 드립니다!

# What we have

- TypeScript, React, React Native 등의 최신 기술을 production 에서 안정적으로 사용한 경험을 갖고 있습니다.
- User 용 iOS/Android application, 상담사용 desktop/iOS application, 관리자용 desktop application 프로젝트의 매주 단위 지속적인 배포 및 운영
- 여러분을 기다리는 약간의 빈자리(...)

## Short-term problem

- Huiseoul 표준이 적용되지 않은 부분에 Huiseoul 표준 적용
  - Component 세분화(react component), state management(mobx-state-tree)나 styling(styled-components) 표준이 적용되지 않은 기존 코드에 적용하기
- Rendering performance 향상
  - Electron, React Native 일부 화면의 rendering performance issue 해결
- 새로운 기능 구현

## Long-term problem

- **빠르고, 예외상황에 잘 대응하는** front-end service 구현
- Front-end 표준 만들기
  - Component, Style, State 을 관리하는 방법의 지속적인 발전
- React Native deep dive 및 contribution
- Performance monitoring & optimization
- Do some open source project of Huiseoul

# What we want you to have

- 조건
  - Typescript(at least javascript ES2015) 로 문제풀이가 가능하신 분
  - 간단한 test case 를 작성할 수 있으신 분 (library 무관)
  - Github/Gitlab 에 계정이 있으며 자신의 코드를 남과 공유해본 경험이 있으신 분
  - Internet browser 에 주소를 입력하고 엔터를 쳤을 때부터 화면이 뜨기까지의 과정을 1분 이상 설명하실 수 있는 분
  - Reading skills in english
  - 한국인 팀 동료와의 의사소통에 문제가 없으신 분
- 우대 조건
  - React, React Native 중 한 가지 이상의 기술이 적용된 서비스 제작 및 운영에 메인으로 참여하신 분
  - React component 의 layout 및 styling 에 자신 있으신 분
  - Open source 프로젝트에 기여하신 경험이 있는 분

![JavaScript-58acbb8a3df78c345bad32c2](https://gitlab.com/huiseoul/handbook/uploads/83848f761fea4085b3221ac6e1d36750/JavaScript-58acbb8a3df78c345bad32c2.jpg)
출처: https://www.thoughtco.com/what-is-javascript-2037921

# Huiseoul front-end stack

- 현재(2018. 02. 22) 기준 production 에서 사용중인 기술 스택입니다.
- Language: [TypeScript](https://www.typescriptlang.org/)
- [React](https://github.com/facebook/react)
- [React Native](https://github.com/facebook/react-native),
- [Electron](https://electron.atom.io/)
- [mobx-state-tree](https://github.com/mobxjs/mobx-state-tree)
- [styled-components](https://github.com/styled-components/styled-components)
- [Jest](https://github.com/facebook/jest)
- [Prettier](https://github.com/prettier/prettier)
- [TSLint](https://github.com/palantir/tslint)

# Process

- 서류 지원
- 온라인 코딩 테스트
- 팀장 면접
- 팀원들과의 [mob coding](https://en.wikipedia.org/wiki/Mob_programming)
  - 서로를 리뷰하는 시간입니다 :)
- 대표와의 면담 및 수습기간(협의 사항)
  - 연봉 및 보상 협의
    - 연봉은 이전 연봉 및 개인별 경력에 따라 상이합니다.
    - 합류 후 퍼포먼스에 맞춰 빠르게 재협상하는 것을 목표로 합니다.
    - 대략적인 범위는 3,000 ~ 7,000 이나 상한은 없습니다.
- 합류

# How to apply

- 다음의 사항을 포함한 pdf 문서를 첨부한 후 engineer@huiseoul.com 로 보내주세요. 24 시간 안에 답장 드리겠습니다.
  - 기본 인적사항
    - 연락처와 github 계정을 꼭 포함해주세요.
  - 프로젝트 경험
  - 자신이 작성한 code 를 확인할 수 있는 주소
    - Open source project 의 PR, 개인 project repository 주소, gist 등
  - 위의 기본 조건, 우대 조건 해당사항에 대한 코멘트
  - 희망 연봉

# Company

- 총 50억 이상의 해외 투자 유치 (비공개 금액 제외)
  - [[1]](http://platum.kr/archives/44865), [[2]](http://platum.kr/archives/68033), [[3]](http://platum.kr/archives/79114), [[4]](http://platum.kr/archives/87363)
- 서울특별시 강남구 논현로 650-1 히아빌딩 6층(학동역에서 도보 5분)

# Contact

- 기타 문의사항은 johnwook.choi@huiseoul.com 으로 부탁드립니다. 12 시간 안에 답장 드리겠습니다.
- 무엇이든 물어봐주세요. 온라인으로 이야기가 어렵다면 직접 만나 차한잔 같이 마시면서 이야기하는 것이 가장 좋아요 :)

# Reference

- [전체 채용 안내](1_index.md)
- [back-end 채용 안내](3_backend.md)
- [Engineering Blog](https://engineering.huiseoul.com)

# Compensation

- 추천을 통해 지원한 지원자가 입사한 날부터 3개월이 되는 때, Apple Watch Series 3 혹은 그에 상응하는 가치의 보상을 추천자와 합격자 모두에게 지급합니다.
