# TL;DR

- 현재 (2018년 3월 27일) 기준, huiseoul 이 backend 영역에서 풀어야 하는 문제에 대해서 정리한 문서입니다.
- 함께 문제를 풀고 싶으신 분들은 [지원](3_backend.md)해주세요!

# Introduction - As-is

![as is architecture](images/as-is-architecture.png)

Huiseoul 은 서비스 오픈 이후 지금까지 소규모(<5) 인원으로 모든 개발을 해왔습니다. 감당할 수 있는 선에서 최소의 규모를 유지하는 것이 중요하다고 생각했기 때문입니다. 매우 큰 불확실성 속에서 스타트업이 가장 우선해야하는 것은 가능한 오래 살아남는 일이니까요!

그래서 huiseoul 의 초기 architecture 는 가능한 적은수의 인원으로 많은 것들을 커버하는 것을 목표로 했습니다. Backend project 하나와 DynamoDB, 그리고 message worker 가 그 시작이었습니다.

서비스가 성장할 수록 조금씩 부채가 생겼습니다. 하나의 프로젝트가 너무 많은 기능을 하게 되었고, 그 위에 코드를 자꾸 추가하다보니 특정한 코드가 필요 이상으로 넓은 범위에 걸쳐 영향을 주게 되었습니다. 기존의 동작을 그대로 모사한 worker 들을 추가하다보니 서로 역할이 겹치는 문제가 생겼고, 서로가 서로를 복잡하게 call 하는 경우도 가지게 되었습니다. 분석을 위해 추가한 RDB 는 DynamoDB 와 중복된 정보를 담는 문제가 있었고, 관리포인트 역시 추가되었습니다.

위 그림은 성장과 이에 따른 부채의 결과로 갖게 된 huiseoul 의 현재 backend architecture 를 간단히 표현한 것입니다.

# To-be architecture

완벽한 아키텍쳐는 없다고 생각합니다. 정답이 있는 문제가 아니라, 상황에 따른 선택의 영역에 있는 문제이기 때문입니다. 현재 huiseoul 의 상황은 이렇습니다. 비지니스의 확장이, 더 빠른 기능 추가가 필요합니다.  이전보다 훨씬 많은 사람들이 함께 일할 수 있어야 합니다. 그러기 위해서는 architecture 를 구성하는 각 부분의 역할을 명확히 하고, 필요 이상으로 결합된 부분을 떼어내야 합니다.

## Part I - API gateway

![to be part 1](images/to-be-part1.png)

현재의 backend project 는 혼자서 너무 많은 기능을 담당하고 있습니다. 이로 인해 발생하는 문제는 크게 두가지 입니다. 첫번째, backend project 의 배포시 배포의 대상이 되는 기능뿐 아니라 다른 동작에도 영향을 준다는 것, 두번째, 코드를 작성할 때 필요 이상으로 넓은 범위에 영향을 주는 코드를 작성하기 쉽다는 것입니다.

좋은 코딩을 결정하는 수많은 기준들 중, 변하지 않는 것은 결합도는 낮게, 응집도는 높게 가져가야 한다는 것입니다. 이를 위해서는 코드 레벨 뿐 아니라 architecture 레벨에서도 역할을 분리하는 것이 필요합니다. 최근 유행하고 있는 microservice 의 장점 중 하나는 구조적으로 낮은 결합도와 높은 응집도를 유도한다는 것입니다. 물론 유도할 뿐, microservice 를 도입한다고 자동으로 결합도와 응집도가 개선되는 것은 아닙니다.

Microservice 의 목적과 관련해서 재미있는 이야기 하나는, microservice 가 필요한 이유 중 하나가 '사람' 때문이라는 것입니다. 하나의 프로젝트가 거대해질 경우 코드의 기능이 필요 이상으로 넓은 범위에 걸쳐지기 쉽습니다. 따라서 누군가가 코드를 변경할 때는 수많은 앞선 작업자들의 결과물을 수정해야 합니다. 수정의 대상이 된다는 것은 피할 일은 아닙니다. 하지만, 필요 이상으로 많은 이해당사자가 생긴다는 것은 불필요한 마찰을 불러옵니다. microservice 는 코드의 소유권한을 분산함으로써 이를 완화시킵니다.

현재 graphql endpoint, socket endpoint, authentication, 3rd party api call 등의 역할을 담당하고 있는 backend project 의 기능을 나누려고 합니다. 이를 위해 api gateway 를 도입하고, 현재 하나의 project 로 되어있는 backend 를 기능에 맞게 여러개의 project 로 나눕니다. 필요에 따라 lambda 나 elasticbeanstalk 등의 aws resource 를 사용합니다.

이로 인해 더 안정적인 배포와 역할의 분리, 더 큰 인원의 참여가 쉬운 기반을 만드려고 합니다.

(* Huiseoul 은 AWS China 를 사용하고 있으며, api gateway 와 lambda 는 2017년 11월에 AWS China 에 추가되었습니다..ㅠ)

## Part II - message queue broker & lambda

![to be part 2](images/to-be-part2.png)

Huiseoul 은 message-queue architecture 를 많이 사용하고 있습니다. 상담 메시지를 저장할 때, 주문의 상태를 변경할 때, 유저의 정보를 저장할 때 등에 이를 사용하고 있습니다. Client 에는 요청의 성공 여부를 빠르게 알려주고, 뒷단의 프로세스는 queue 에 맡깁니다. message-queue architecture 를 사용하는 이유는 다음과 같습니다. 첫번째, caller 와 callee 의 decoupling 을 유도하고, 둘째, 가능한 모든 부분에 async architecture 를 적용하여 scability 확보하기 위해서입니다.

이는 message worker 로부터 시작되었습니다. 실시간 상담은 socket layer 에서 처리합니다. 이 때, 나중을 위한 message 의 저장은 queue 에 맡깁니다. Worker 는 queue 를 consume 하고, 처리에 실패한 메시지는 다시 queue 에 집어 넣음으로써 일단 queue 에 성공적으로 쌓인 메시지는 매우 높은 확률로 처리 완료를 장담할 수 있습니다.

message worker 이후, 정보를 잃어버리지 않으면서도 async 하게 처리해야하는 일들이 추가되었습니다. 기존의 message worker 의 동작을 모사한 order worker 나 user worker 를 추가했습니다. 추가해야하는 동작들 중 어느 worker 에 속하는 지가 명확하지 않은 일들이 생기기 시작했습니다. 각 worker 가 서로를 참조해야 하는 일이 생기기 시작했습니다.

message-queue-worker 의 주 기능은 decoupling 과 retry policy 에 있습니다. 따라서 여러개의 message-queue-worker 가 이를 나눠서 담당할 필요는 없습니다. worker 내에서의 코드 역시 part 1과 마찬가지 이유로 역할의 분리가 필요합니다. 그래서 하나의 message-queue-worker 를 사용하고, worker 에서는 lambda 를 call 하게 하려고 합니다.

## Part III - DynamoDB & ElasticSearch

![to be part 3](images/to-be-part3.png)

Huiseoul 은 개발 초기부터 DynamoDB 를 적극 활용했습니다. DB 를 관리하고 scaling 하는 것이 쉬운 일이 아니기 때문에, 관리포인트를 최소한으로 하고 싶었습니다. DynamoDB 의 사용성과 속도에는 매우 만족했지만, query 의 자유도는 포기했습니다.

비즈니스가 성장할수록 query 에 대한 요구는 자연스레 높아졌습니다. 이를 해결하기 위해 가장 익숙한 방식을 택했고, 그래서 RDB(중 PostgreSQL)를 추가했습니다. 당시에 문제를 해결할 수 있는 가장 빠른 방법이었습니다. 저장하는 data 의 종류가 다양해지고, 그 양이 커질수록 현재의 구조는 문제가 됩니다. 같은 정보를 중복으로 담아야 하고, 각 DB 의 역할이 명확하지 않기 때문입니다. Query 의 성능 역시 문제가 될 수 있습니다.

![dynamodb autoscaling](images/dynamodb-autoscale.png)

DynamoDB 의 read/write 성능과 autoscaling 기능은 유지하면서, 다양한 분석에 대처하고자 합니다. 가장 유력한 solution 으로  elasticsearch 을 선정했습니다. field 에서 검증된 성능과 안정성을 가졌기 때문입니다. 

# Outro

![to be architecture](images/to-be.png)

Huiseoul 은 꾸준히 성장하고 있습니다. 서비스와 비지니스의 성장에 발맞춰 architecture 역시 성장합니다. 함께 성장해나갈 [엔지니어를 찾고 있습니다.](3_backend.md) 이외에도 서비스의 각 부분에 대한 monitoring, 자동화, data 분석, elasticsearch 운영, machine learning 등에 관심있으신 모든 분들의 연락을 기다리고 있습니다!


