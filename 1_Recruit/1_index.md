![image](https://gitlab.com/huiseoul/handbook/uploads/4f202d52948217f94c38a0df634d0bb4/image.png)
출처: https://www.apple.com/shop/buy-watch/apple-watch/gold-aluminum-pink-sand-sport-band

# TL;DR

- Huiseoul 과 함께할 **신입**/경력 front-end/back-end engineer 를 찾습니다.
- 주변에 있는 좋은 분들을 소개해 주세요! 입사가 확정되면 **추천하신 분과 추천받으신 분 모두**에게 애플 워치3를 드립니다!

# Huiseoul?

우리는 [Huiseoul](http://www.huiseoul.com/) 이라는 서비스를 만들고 있습니다. 가장 단순히 표현하면, Huiseoul 은 중국에서의 한국 화장품 직구를 위한 서비스입니다. 하지만 일반적인 쇼핑몰은 아닙니다. 피부 고민에 맞는 최적의 화장품을 전문가의 상담을 통해 가장 빠르게 구매할 수 있는 서비스입니다.

많은 사람들이 [#ConvComm](https://medium.com/chris-messina/2016-will-be-the-year-of-conversational-commerce-1586e85e3991) 이라는 단어에 주목했습니다. 세계 곳곳에서 [messaging](https://medium.com/@Operator/the-messaging-future-is-here-and-its-going-to-change-everything-99e29bcff68e) 을 기반으로 한 [다양한](http://adage.com/article/digital/wechat-teaches-future-social-commerce/306765/) [시도](http://www.koreaherald.com/view.php?ud=20170215000895)도 있었습니다(지금도 계속되고 있구요). 대부분의 시도는 Facebook messenger 나 kik 등의 플랫폼 위에서 user의 몇 가지 command 에 대응할 수 있는 bot 을 만드는 일이었습니다. [여전히 기대중이라는 사람](https://medium.com/chris-messina/2016-will-be-the-year-of-conversational-commerce-1586e85e3991)도 있고 [이제는 끝났다는 의견](http://www.looah.com/article/view/2061)도 있습니다.

Bot 은 틀렸을 지 몰라도 messaging 은 틀리지 않았다고 생각합니다. 사람들은 모바일로 좋은 상품을 믿고 편하게 구매하고 싶어합니다. 이런 구매자와 판매자 사이의 가장 뛰어난 연결고리는 ‘대화'입니다. 지구에서 처음 구매자와 판매자 사이에 거래가 생긴 날 이래, 사람이 사람에게 물건을 산다는 사실은 바뀌지 않았습니다. 바뀌지 않는 사실위에 기술이라는 날개를 다는 것이 우리가 하는 일입니다.

![image](https://user-images.githubusercontent.com/2437909/32874444-a60d737c-cad5-11e7-9192-9daafbc5ec80.png)

- 중국 시장에 [Android/iOS app 을 출시](https://github.com/facebook/react-native/pull/9807)했습니다.
- 전문가와의 상담을 통한 한국 화장품 역직구라는 비즈니스 모델을 증명했습니다. 
- 보세창고를 통한 정식 수출, 고객용 mobile application, 상담사용 desktop/mobile application 을 운영했습니다.
- 최근 6개월 동안 20배 정도의 외형적 성장을, 내적으로는 scale up 을 할 수 있는 기술적 성장을 만들었습니다.

# What we have

- TypeScript, React, React Native, GraphQL 등의 최신 기술을 production 에서 안정적으로 사용한 경험을 갖고 있습니다.
- 무엇인가를 더 제공하기보다 일에 방해가 되는 일들을 제거하는 것에 더 집중합니다. ex)야근, 잦은 회의
- 망하지 않을 만큼의 자원, 대(對)중국 및 중국 내 시스템의 안정적인 오퍼레이션 경험, 함께 한 3년
- 여러분을 기다리는 약간의 빈자리(...)


# Jobs

- [Front-end 채용 안내](2_frontend.md)
- [Back-end 채용 안내](3_backend.md)

# Process

- 서류 지원
- 온라인 코딩 테스트
- 팀장 면접
- 팀원들과의 [mob coding](https://en.wikipedia.org/wiki/Mob_programming)
  - 서로를 리뷰하는 시간입니다.
- 대표와의 면담 및 수습기간(협의 사항)
  - 연봉 및 보상 협의
    - 연봉은 이전 연봉 및 개인별 경력에 따라 상이합니다.
    - 합류 후 퍼포먼스에 맞춰 빠르게 재협상하는 것을 목표로 합니다.
    - 대략적인 범위는 3,000 ~ 7,000 이나 상한은 없습니다.
- 합류

# Company

- 총 50억 이상의 해외 투자 유치 (비공개 금액 제외)
  - [[1]](http://platum.kr/archives/44865), [[2]](http://platum.kr/archives/68033), [[3]](http://platum.kr/archives/79114), [[4]](http://platum.kr/archives/87363)
- 3월 초 서울특별시 강남구 논현로 650-1 히아빌딩 6층으로 이사 예정(학동역에서 도보 5분)

# Contact

- 무엇이든 물어보세요! johnwook.choi@huiseoul.com 으로 부탁드립니다. 12 시간 안에 답장 드리겠습니다.
- 온라인으로 이야기가 어렵다면 직접 만나 차한잔 같이 마시면서 이야기하는 것도 좋아요 :)

# Reference

- [front-end 채용 안내](2_frontend.md)
- [back-end 채용 안내](3_backend.md)
- [Engineering Blog](https://engineering.huiseoul.com)

# Compensation

- 추천을 통해 지원한 지원자가 입사한 날부터 3개월이 되는 때, Apple Watch Series 3 혹은 그에 상응하는 가치의 보상을 추천자와 합격자 모두에게 지급합니다.
