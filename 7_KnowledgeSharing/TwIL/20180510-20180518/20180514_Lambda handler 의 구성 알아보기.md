2018 년 5 월 14 일 @johnwook

## Introduction

- Node.js 기준
- Lambda 는 정의한 handler code 대로 실행됩니다.
- Handler code 의 구성에 대해서 알아봅니다.

## Handler 의 기본 형태

```javascript
exports.handler = (event, context, callback) => {
  const successfulResult = doSomething();
  callback(null, successfulResult);
};
```

## Handler function 의 argument

### event

- Lambda trigger 에 따라 다릅니다.
  - lambda 를 invoke 를 통해서 직접 실행했다면, `event` 는 invoke 시에 넘긴 Payload property(를 parse 한 버전)이 됩니다.
  - SNS 가 trigger 라면 `event` 는 `event.Records[0].Sns.Message` 와 같은 정보를 포함합니다.
  - 따라서 lambda 의 trigger 에 따라 어떤 형태의 input 이 들어오는 지 확인해야 합니다.

### context

- 사용해 본 적이 없습니다...
- [참조문서](https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html)

### callback

- 결과 callback 입니다.
- 첫번째 인자는 error 입니다. 실행이 성공할 경우 null 을 설정하면 됩니다.
- 두번째 인자는 `성공` 여부와 함께 전달할 parameter 를 설정합니다.
  - e.g. 함수의 실행 결과
  - JSON.stringify compatible 해야 합니다.
  - 첫번째 인자가 있을 시 두번째 인자는 무시됩니다.

## TypeScript 적용 및 기본 template 구성

- 각 함수별로 dir 을 만들도록 했습니다. (lambdas project 참조)
- index.js 파일은 위 기본 handler 형태로 두고, handler 에서 bundled ts 파일을 사용하도록 했습니다.
  - best way 는 아닌 것 같아요. 더 정리해내면 좋겠습니다.

## 참조문서

- https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-handler.html
- https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-mode-exceptions.html
