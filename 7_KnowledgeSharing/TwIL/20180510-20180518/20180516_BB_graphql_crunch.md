2018 년 5 월 16 일 @bobeenlee

## Introduction
Graphql JSON 데이터 통신에서 발생하는 중복된 데이터를 줄여주고 압축하여 개선해준다. <br/>
사실 Graphql Response가 아니여도 기존 API도 이용가능하다. <br/>
<br/>
payload의 양을 줄여 모바일이나 데이터 통신이 원활하지 않은 곳에 작은 사이즈로 통신이 가능하게 할 수 있다. <br/>
[JSON 데이터 예시](https://github.com/banterfm/graphql-crunch/blob/master/readme.md#usage)

## Usage
```
npm install graphql-crunch --save
```

#### Client
* 서버에서 응답 데이터를 받은 후 uncrunch로 crunch된 데이터를 읽을 수 있게 구성한다.
```javascript
    fetch(...).then(response => uncrunch(response));
```

#### Server
* express, koa등등 서버에서 응답데이터를 주기전에 crunch함수로 중복된 데이터 제거, 압축한다.
* middleware로 crunch하여 보냅니다.

자세한 내용은 [가이드참조](https://github.com/banterfm/graphql-crunch/blob/master/readme.md#usage)

## Example
* https://github.com/BoBinLee/crunch-example
* 서버는 [Prisma](https://www.prisma.io/)
* 클라이언트 [React App](https://reactjs.org/)

## Reference
* [deduplicator and crunch](https://github.com/prismagraphql/graphql-yoga/issues/303#issuecomment-386383356)
* https://github.com/banterfm/graphql-crunch
