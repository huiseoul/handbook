2018 년 5 월 25 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# Lambda Local Test

AWS Lambda Function 개발 시 Local 환경에서 테스트 해보기 어렵다는 단점이 있습니다. 적절한 테스트 없이 운영 환경에 직접 배포할 경우 프로젝트 안정성을 낮출 수 있기 때문에 Lambda Local Test 방법을 공유하도록 하겠습니다.

## [SAM Local](https://aws.amazon.com/ko/blogs/korea/new-aws-sam-local-beta-build-and-test-serverless-applications-locally/)

AWS 에서 제공하고 있는 Serverless 개발도구입니다. Lambda 뿐만 아니라 S3, DynamoDB, SNS, API Gateway 등 Serverless 구성과 관련된 서비스와 통합 시뮬레이션이 가능합니다. 하지만 현재 Huiseoul 에서 사용중인 AWS 서비스들을 고려했을 때 오버스펙이라 판단하여 SAM Local 에서 내부적으로 사용하고 있는 docker 이미지를 활용하여 필요한 부분만 커스터마이징 하기로 했습니다.

## [lambci/lambda Docker Image](https://hub.docker.com/r/lambci/lambda)

lambci/lambda 는 실제 AWS Lambda 와 유사하게 구성한 Docker Image 입니다. (100% 일치라고 보기는 어렵습니다.) AWS 에서 만들어 제공하는 건 아니지만 잘 만들어졌다고 판단했는지, [SAM Local 에서 해당 이미지를 사용](https://github.com/awslabs/aws-sam-cli#a-special-thank-you)하고 있습니다.

사용은 간단합니다. 개발한 lambda function directory 에서 `docker run`하면 됩니다. 실행 결과 및 로그는 실제 Lambda 와 거의 유사합니다. Error 도 잘 표기합니다.

```
$ docker run --rm -v "$PWD":/var/task lambci/lambda:nodejs8.10 {lambdaHandlerIndex} {lambdaFunctionParams}
```

다만 Local DynamoDB 와 연결하여 테스트할 경우, Docker 이미지 내에서 DynamoDB 를 실행하기 때문에 `localhost:7000`과 같은 endpoint 가 유효하지 않습니다. 이 때 env 환경변수로 local IP 를 넘겨주고, 코드에서 이를 사용하는 방식으로 구현 가능합니다.

* 실행 명령

```
docker run --rm -e IP=${localIP} -v "$PWD":/var/task lambci/lambda:nodejs8.10 {lambdaHandlerIndex} {lambdaFunctionParams}
```

* lambda function 구현체 내 dynamoDB options 설정

```typescript
const dynamodb = new DynamoDB({
  endpoint:
    process.env.NODE_ENV === "production"
      ? productionEndpoint
      : `http://${process.env.IP}:7000`
  ...
});
```
