# Apollo Server 2

저희는 `"graphql-server-express": "^0.7.1"`를 사용하고 있습니다.

현재 Apollo Server 2 는 rc 버전으로 추가/변경/삭제 될 요소가 많습니다.

[진행사항](https://github.com/apollographql/apollo-server/blob/master/ROADMAP.md)

`npm install --save apollo-server@rc graphql`

## What's New

### Default GraphQL Playground

이제 따로 graphiql 을 설정하거나 Desktop 프로그램이 없어도 루트(`http://localhost:4000`)로 접속하면 Graphql Playground 가 실행됩니다.

### Health Checks

기본적으로 `http://localhost:4000/.well-known/apollo/server-health`으로 접속하면 Health Check 를 합니다.

### Error Handling

GraphQL 내부적으로 발생하는 오류는 ApolloError 로 처리합니다.

```js
const { ApolloServer, gql, AuthenticationError } = require("apollo-server");

const typeDefs = gql`
  type Query {
    authenticationError: String
  }
`;

const resolvers = {
  Query: {
    authenticationError: (parent, args, context) => {
      throw new AuthenticationError("must authenticate");
    }
  }
};

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
```

### Logging

로그를 포멧별로 저장 할 수 있습니다.

```js
const server = new ApolloServer({
  typeDefs,
  resolvers,
  formatParams: params => {
    console.log(params);
    return params;
  },
  formatError: error => {
    console.log(error);
    return error;
  },
  formatResponse: response => {
    console.log(response);
    return response;
  }
});
```

### WebSocket (Comming Soon!)

WebSokect 연결을 subscriptions 연결로 바꿔서 처리해주는 라이브러리가 추가 될 예정입니다.

[subscriptions-transport-ws](https://github.com/apollographql/subscriptions-transport-ws)

### apollo-server-express Quick start

```js
const express = require("express");
const { ApolloServer, gql } = require("apollo-server-express");

const typeDefs = gql`
  type Query {
    hello: String
  }
`;

const resolvers = {
  Query: {
    hello: () => "Hello world!!!"
  }
};

const server = new ApolloServer({ typeDefs, resolvers });

const app = express();
const path = "/graphql";
server.applyMiddleware({ app, path });

app.listen({ port: 4000 }, () =>
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`)
);
```
