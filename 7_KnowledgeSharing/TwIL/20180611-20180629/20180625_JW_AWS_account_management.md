# AWS Credentials

## 1. AWS credentials profiles

- 관리해야하는 계정이 많아질수록 aws cli 의 profile option을 사용하는 것이 편리합니다.

- credentials 관리 `vi ~/.aws/credentials` (없다면 생성)

  - ```bash
    [profile-1]
    aws_access_key_id = ACCESS_KEY_1
    aws_secret_access_key = SECRET_1
    [profile-2]
    aws_access_key_id = ACCESS_KEY_2
    aws_secret_access_key = SECRET_2
    ```

- credentials 사용

  - 모든 aws cli 명령 및 api는 profile을 지정할 수 있도록 되어 있습니다.
    - `aws s3 ls --profile profile-1`

## 2. AWS production account

- 보안, 의도치 않은 data 조작 방지를 위해서 production account를 분리해서 운영하는 것이 best practice
  - 중국에서는 안됨...

## 3. Cross-account access

![imgae](https://docs.aws.amazon.com/IAM/latest/UserGuide/images/tutorial-cross-accounts.png)

- development 계정이 production 계정의 log를 보거나 일부 권한(e.g. read)을 갖는 경우가 필요하기 때문에, cross-account 권한 설정이 가능함
