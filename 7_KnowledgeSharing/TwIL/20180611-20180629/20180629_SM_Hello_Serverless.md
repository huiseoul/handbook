2018 년 6 월 29 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# [Serverless](https://serverless.com/)

## What`s Serverless

- Serverless Architecture 를 구성하기 쉽도록 배포 lifeCycle 을 제공하는 toolkit
- AWS 뿐만 아니라 Azure, Google Cloud Platform, IBM OpenWhisk 등 다양한 Cloud Provider 서비스를 지원
- Huiseoul 은 AWS 를 사용하므로 AWS 를 기반으로 서술하겠습니다.

---

## [Serverless CLI](https://serverless.com/framework/docs/providers/aws/cli-reference/)

- 설치

```
$ npm install -g serverless
```

- [다양한 boilerplate 지원](https://serverless.com/framework/docs/providers/aws/guide/services#creation)

```
// node-typescript 예시
// .ts -> .js 컴파일 및 번들링까지 기본 지원
$ serverless create --template aws-nodejs-typescript --path hello-serverless
```

- 세 가지만 알면 된다

```
// service 배포
$ serverless deploy

// lambda 개별배포
$ serverless deploy funcfion -f {functionName}

// invoke
$ serverless invoke [local] -f {functionName} -l
```

---

## [serverless.yml](https://serverless.com/framework/docs/providers/aws/guide/)

- `serverless.yml` 파일 하나로 serverless 환경 구축을 위한 모든 설정이 가능
  - AWS Lambda preferences
  - AWS API Gateway event 설정
  - AWS IAM Role 설정
  - 기타 등등...

```yml
service:
  name: worker

// 서비스 하위에 공통 적용
provider:
  name: was
  runtime: nodejs8.10
  region: cn-north-1
  profile: china # local profile 지정 가능
  iamRoleStatements:
    - Effect: Allow
      Action: # Gives permission to DynamoDB tables in a specific region
        - dynamodb:DescribeTable
        - dynamodb:Query
        - dynamodb:Scan
        - dynamodb:GetItem
        - dynamodb:PutItem
        - dynamodb:UpdateItem
        - dynamodb:DeleteItem
      Resource: "arn:aws:dynamodb:us-east-1:*:*"

# 서비스별 개별적용 (우선순위 더 높음)
functions:
  hello:
    handler: handler.hello
    onError: arn:aws:sns:… # SNS 지원중, SQS 미지원(개발중)
  sunmi:
    handler: sunmi/handler.sunmi # 이렇게 directory 구성을 하면 여러 lambda worker group을 관리하기에 용이
    name: ${self:service.name}-sunmi
    events:
      - http:
          method: get
          path: sunmi
```

---

## 결론!!

- 환경변수 설정 이슈로 China region 을 지원하지 않는 서비스들이 많은데, 갈증을 해소해준다!
- 생각보다 세세하게 설정할 수 있다.
- 다른 서비스(`apex`, `up`)와 지원 내용은 크게 다르지 않은 듯!
- 배포 시 변경된 부분만 배포된다.
- 같은 service group 내에 function 을 추가하거나 제외하면 실제로 lambda function 도 추가되거나 제거되어 group 화하여 관리하기 용이하다.
