2018 년 6 월 15 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# Vim

---

## Mode

- Normal (`esc`)
  - 일반/명령
  - `:`, `/`: 명령어
- Insert (`a/i/o/s`)
  - 입력/편집
- Visual (`v/V`)
  - 선택

---

## Normal

- `esc`로 진입
- 방향

  ![](https://c7.staticflickr.com/9/8138/29831678110_39867512ab_n.jpg)

- `:`를 누르면 명령어 모드로 진입
  - vim 액션 명령
  - 파일 저장/종료 (`w/q` + `!`)
  - 파일 열기(`edit, tabnew`)
  - 탭/버퍼 관련 명령
  - 쉘 명령 실행 등(`!`)

---

## 이동

![vimMove](http://cfile8.uf.tistory.com/image/141F57474F4732890DE706)

- 다 외우려니까 심란하다. `0, ^, $`, `w, b, e`, `H, L`, `gg, G`, `Ctrl+b/f`, `?, /text`

---

![vimCommandsAll](http://i0.wp.com/www.insightbook.co.kr/wp-content/uploads/2012/04/Vim_%EB%AA%85%EB%A0%B9%EC%96%B4_%EB%8B%A8%EC%B6%95%ED%82%A4.jpg?zoom=2&resize=614%2C450)

---

## 요거이 써보니까 편하더라!

- Ctrl+v & Shift+i

  ![block](https://cdn.huiseoul.com/needHello.png)

---

- 튜토리얼로 친해지자!
  - http://www.openvim.com/tutorial.html
- 글로 배워보자!
  - http://www.mimul.com/pebble/default/2014/07/15/1405420918073.html
- 게임으로 즐기자!
  - https://vim-adventures.com/
