2018 년 6 월 22 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# [AWS API Gateway](https://aws.amazon.com/ko/api-gateway/)

## AWS API Gateway?

- Amazon API Gateway 는 어떤 규모에서든 개발자가 API 를 손쉽게 생성, 게시, 유지 관리, 모니터링 및 보호할 수 있도록 지원하는 완전관리형 서비스입니다.
  - 라고 공식문서에 적혀있지만 무슨 말인지 이해하기 어렵다.
- 모든 클라이언트 요청의 endpoint 를 하나로 통합하는 서비스 (like Router)
  - Micro Service Architecture 에서 서비스 도메인을 하나로 통합하는 개념
  - endpoint 가 추가되거나 변경되었을 때 client 가 모든 endpoint 를 세세하게 인지해야한다는 단점을 극복!

---

## 실험(Lambda + API Gateway) & 시행착오

- 다양한 방법으로 가능하다
  - Lambda Function 생성 시에 API 를 함께 만들 수도 있고
  - Lambda Function 과 API 를 따로 만들고 연결할 수도 있다
- 시행착오 1

  - Lambda Function 생성시에 Lambda Console 을 따라 API 를 만들면 `ANY` 메소드로 API endpoint 가 생성된다.
  - 그리고 Lambda Function 을 실행하면 아래와 같은 에러를 만날 수 있다.
  - 당황하지 말고, API Gateway Console > Resources > method 선택 > Integration Request > Use Lambda Proxy integration 설정을 살펴보자
  - [자세한 설명](https://medium.com/@lakshmanLD/lambda-proxy-vs-lambda-integration-in-aws-api-gateway-3a9397af0e6d)

```
{"message": "Internal server error"}
```

- 시행착오 2
  - 그래도 같은 에러가 난다.
  - API Gateway `Deploy API`를 꼭 하자.
  - 별거 아닌거 같은데 정말 이상하게 시간을 잡아먹을 수 있다. 꼭 deploy 를 하자.

---

## 결론

- 아직 실무에 써보지는 않아 잘 모르지만 서비스를 통하는 문이 하나라는건 client 입장에서 편하다!
- 서비스로 통하는 단일 관문이니 firewall 의 역할도 하면 좋겠지만 다른 서비스를 붙여하는 것으로 보인다.
  - [관련 내용](https://aws.amazon.com/ko/blogs/compute/protecting-your-api-using-amazon-api-gateway-and-aws-waf-part-i/)
  - 차선으로 Log 를 쌓고 수동으로 정책을 만드는 방법이 있을듯 싶다.
- 위 시행착오를 제외하면 AWS Console 이 시키는대로 클릭하면 거의 잘 된다.
