# HuiSeoul Worker Structure

![huiseoul worker structure](https://gitlab.com/huiseoul/lambdas/uploads/edebcd232e7e5860719e722b41a1723c/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA_2018-06-26_%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE_3.17.53.png)

### 후보군

- Lambda [AWS-Sample](https://github.com/aws-samples/aws-lambda-fanout/blob/master/README.md)
  - Easy to Test
  - Hard to Error Handling
- 1 개의 SQS
  - SQS 단독으로 쓰기에는 많은 OverHead

### SNS 의 장점

- [메세지의 전송을 보장](https://stackoverflow.com/questions/30750033/amazon-sns-delivery-retry-policies-for-sqs)
- 메시지를 전송 확인 가능
- CloudWatch
- Error Handling

### SNS 의 단점

- 중복 발행
- 메세지의 순서를 보장하지 않음

#### 메시지 전송 상태 확인 목록

- 메시지가 Amazon SNS 엔드포인트에 전송되었는지 확인
- Amazon SNS 엔드포인트에서 Amazon SNS 로 전송된 응답 식별
- 메시지 유지 시간(게시 타임스탬프 시간부터 Amazon SNS 엔드포인트에 넘겨주기 직전까지의 시간) 결정

### Retry

- Lambda 를 사용할 수 없는 경우, SNS 는 1 초 간격으로 2 번, 1 초에서 20 분까지 **지수적** **백오프**로 10 번, 마지막으로 20 분 간격으로 38 번을 재시도하여, 13 시간 넘는 시간 동안 총 50 번 재시도한 후 메시지를 SNS 에서 폐기합니다.

### BackOFF

- [**Number of retries**]로 선택한 값은 [**Retries with no delay**], [**Minimum delay retries**] 및 [**Maximum delay retries**]에 대해 설정한 재시도를 포함하여 총 재시도 횟수를 나타냅니다.

  백오프 단계에서 세 가지 매개 변수를 사용하여 재시도 간격을 제어할 수 있습니다.

  - Minimum delay—이 최소 지연은 백오프 단계에서 처음 시도하는 재시도에 대한 지연을 정의합니다.
  - Maximum delay—이 최대 지연은 백오프 단계에서 마지막으로 시도하는 재시도에 대한 지연을 정의합니다.
  - Retry backoff function—재시도 백오프 함수는 Amazon SNS 가 백오프 단계의 첫 재시도부터 마지막 재시도까지의 모든 재시도에 대한 지연을 계산하는 데 사용할 알고리즘을 정의합니다.

![](https://docs.aws.amazon.com/ko_kr/sns/latest/dg/images/sns-delivery-policy-phases.png)

![](https://docs.aws.amazon.com/ko_kr/sns/latest/dg/images/backoff-graph.png)

### 전송량

최대 256kb 이지만 보통은 64kb 까지만 보낼 수 있음 64~256 은 SigV4 검증을 하면 사용 가능

## Refernece

[SNS format](https://utoolity.atlassian.net/wiki/spaces/UAAWS/pages/104530386/Publish+SNS+Message+-+structured+JSON+format)

[ Benchmarking Amazon SNS & SQS For Inter-Process Communication In A Microservices Architecture](https://hackernoon.com/benchmarking-amazon-sns-sqs-for-inter-process-communication-in-a-microservice-architecture-de0dfa8d6ac6)

[Fan out With Lambda](https://theburningmonk.com/2018/04/how-to-do-fan-out-and-fan-in-with-aws-lambda/)

[Simple note](https://aws.amazon.com/ko/blogs/aws/queues-and-notifications-now-best-friends/)

[Amazon SNS 로 지속적 관리가 가능한 대용량 푸쉬 시스템 구축 여정 - AWS Summit Seoul 2017](https://www.slideshare.net/awskorea/amazon-sns-push-service)

[SNS VS Kinesis](http://tech.zooplus.com/event-driven-using-aws-infrastructure-design/)

[Lambda proxy](https://docs.aws.amazon.com/ko_kr/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html)
