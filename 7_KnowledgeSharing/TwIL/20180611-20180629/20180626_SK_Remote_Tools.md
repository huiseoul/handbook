# 원격 근무 시 도움 받을 수 있는 도구들

## Communication

- [Trello](https://trello.com)
- [Slack](https://slack.com)

## 시간

- [Every Time Zone](https://everytimezone.com/#2018-6-21,45,b8jj)
- Mac weather widget

## 화이트보드 공유

- [Realtime Board](https://realtimeboard.com/signup/)
- [AWW](https://awwapp.com/pricing/)
- [Google Slides](https://www.google.com/intl/ko_kr/slides/about/)

## 화상회의

- [appear.in](https://appear.in/)
- [zoom](https://zoom.us/)

## 협업 코딩

- [VS Live Share](https://visualstudio.microsoft.com/ko/services/live-share/)
- [Cloud9](https://aws.amazon.com/ko/cloud9/)
