# Yarn Workspace, Lerna을 이용하여 React, React Native 코드 공유하기

## 배경

Web과 App사이에 core, ui의 공통 코드를 공유하기 위한 방법이 필요하였다.

## Yarn Workspace
yarn workspace로 React Native, React 코드를 공유하기 위해선 [구조 설정과 약간의 세팅이 필요하다.](https://github.com/viewstools/yarn-workspaces-cra-crna)<br/>


## [Lerna](https://github.com/lerna/lerna)
Lerna는 lerna.json으로 따로 설정만하고 packages폴더로 구분만하면 패키지들끼리 임포트하여 공유가능하다. <br/>
yarn workspace보다 설정이 간단하여 쉽게 적용할 수 있다. <br/>
* https://github.com/lerna/lerna#getting-started

### Example 
* https://github.com/agrcrobles/react-native-web-workspace


## Lerna를 이용하자
Yarn Workspace는 설정도 그렇고 React에서 아직까진 사용하기 힘들고 Lerna는 lerna설정만 따로 해주면 되니 심플하다.<br/>
웹과 앱 공동 개발 시 유용할 듯 싶다.
