# Backend to-be architecture

![image-20180604092820999](https://gitlab.com/huiseoul/handbook/uploads/bc82947ba44f69b9e90267e6e010162b/image.png)

# EC2 vs Lambda

|                  | ElasticBeanstalk(EC2)                                        | Lambda                  |
| ---------------- | ------------------------------------------------------------ | ----------------------- |
| Scale way        | horizontal: number of instance<br />vertical: type of instance | concurrency             |
| Scale management | auto scaling                                                 | -                       |
| Uptime           | -                                                            | cold & warm start       |
| Codebase         | normal code                                                  | lambda specific handler |

1. Lambda specific codebase
2. cold & warm start

# Apex Up

## Lambda specific codebase

- Abstraction - code level
  - Just write pure javascript server code
  - Easy testing
- Abstraciton - infrastructure level
  - API gateway(stage), IAM policies, Route53, ...

### Cold & warm start

- [Active warming](https://up.docs.apex.sh/#configuration.lambda_settings.active_warming)
  - Up pro plan
  - `warm` – Enable active warming (Default: false)
  - `warm_count` – Number of concurrent containers to warm (Default: `15`)
  - `warm_rate` – Rate at which to perform the warming (Default: `"15m"`)

# Conclusion

![image-20180604092820999](https://gitlab.com/huiseoul/handbook//uploads/7aad37589b3f61e1a49ae477286f9001/image.png)

- `Apex up` worth it
