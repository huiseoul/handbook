2018 년 5 월 28 일 @johnwook

# Introduction

- 최근 서비스에 lambda 를 도입하면서, lambda management 에 대한 필요성이 생겼습니다.
- [up](https://github.com/apex/up) 을 한번 실행해 봅시다.

# Install

```bash
$ curl -sf https://up.apex.sh/install | sh
```

# AWS credentials

- Best parctice 는 profile 을 이용하고, `up.json` 에 사용하는 profile 을 명시하는 것입니다.
- 공동 작업을 위해서는 team level 로 profile 명을 정하는 것이 필요하겠습니다.

# Getting started

- 동작을 위해 필요한 파일은 두개입니다.
  - `up.json`, `app.js`
- 문서에 맞게 두 파일을 생성하고, `up` 을 치면 끝!

# Tear down

- `up stack delete` 을 수행해서 리소스를 지웁니다.

# 소감

- lambda handler 가 독특해서 항상 test 등이 고민이었는데, lambda 를 꽤나 훌륭하게 추상화하고 있어서 production 에 적용하면 좋겠습니다.
