2018 년 6 월 1 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# [Apollo Engine](https://www.apollographql.com/docs/engine/)

- GraphQL gateway
  - provides caching
  - [performance monitoring](https://engine.apollographql.com/account/gh.SeeArtSun)
  - and so on..

* +testProject: https://github.com/SeeArtSun/typescript-micro-graphql-boilerplate

---

# 실습!

- test server 를 띄워봅니다.
- http://127.0.0.1:8088/graphql 에서 query 실행!

```
query {
  book(isbn: "1"){
    title
    author
  }
}
```

- 실행결과: 쿼리 결과 뿐만 아니라 tracing 및 cacheControl 결과도 함께 보여주고 있습니다.

```
{
  "data": {
    "book": {
      "title": "Harry Potter and the Sorcerer's stone",
      "author": "J.K. Rowling"
    }
  },
  "extensions": {
    "tracing": {
      "version": 1,
      "startTime": "2018-06-01T01:33:49.035Z",
      "endTime": "2018-06-01T01:33:49.035Z",
      "duration": 525551,
      "execution": {
        "resolvers": [
          {
            "path": [
              "book"
            ],
            "parentType": "Query",
            "fieldName": "book",
            "returnType": "Book",
            "startOffset": 440401,
            "duration": 18545
          },
          {
            "path": [
              "book",
              "title"
            ],
            "parentType": "Book",
            "fieldName": "title",
            "returnType": "String",
            "startOffset": 485714,
            "duration": 9330
          },
          {
            "path": [
              "book",
              "author"
            ],
            "parentType": "Book",
            "fieldName": "author",
            "returnType": "String",
            "startOffset": 507034,
            "duration": 6982
          }
        ]
      }
    },
    "cacheControl": {
      "version": 1,
      "hints": [
        {
          "path": [
            "book"
          ],
          "maxAge": 60
        }
      ]
    }
  }
}
```

- https://engine.apollographql.com/ 에 접속하여 GUI 로도 확인해봅니다.
  - Top Request
    ![TopRequest](https://cdn.huiseoul.com/00_apolloEngineTopRequest.png)
  - Performance Overview
    ![PerformanceOverView](https://cdn.huiseoul.com/01_apolloEngineDashboard.png)
  - Request Rate
    ![RequestRate](https://cdn.huiseoul.com/02_requestRate.png)
  - Request Latency
    ![RequestLatency](https://cdn.huiseoul.com/03_requestLatency.png)
  - Error Tracing
    ![ErrorTracing](https://cdn.huiseoul.com/04_errorTracing.png)
  - Schema
    ![Schema](https://cdn.huiseoul.com/05_schema.png)

---

# From a Huiseoul point of view

- Error Tracking
- Query & Mutation 사용률 분석
- Query & Mutation Performance 개선
- Caching 을 통한 자원절약 및 응답속도 향상
  - ex) Products, HuiseoulContents...
