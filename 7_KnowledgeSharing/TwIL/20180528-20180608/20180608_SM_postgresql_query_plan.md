2018 년 6 월 8 일 [SunKim](https://medium.com/@seeart.sun) TwIL

# [QUERY EXPLAIN PLAN](https://www.postgresql.org/docs/9.3/static/sql-explain.html)

---

## EXPLAIN

- 실제 수행 안함

## EXPLAIN ANALYZE

- 실제로 수행

---

## 사용법

```sql
$ EXPLAIN SELECT * FROM TABLE_NAME WHERE ...;
```

## Options

- Format { TEXT | XML | JSON | YAML }

```
$ EXPLAIN (Format json) SELECT ... ;
```

- ANALYZE / VERBOSE / COSTS(default) / BUFFERS / TIMING(default)

```
$ EXPLAIN (ANALYZE, COSTS false) SELECT ... ;
```

---

## [실행결과](https://www.postgresql.org/docs/9.1/static/using-explain.html)

- Scan: 사용된 scanning 방법

- Rows: Fetch 수

- Width: Column

- Cost: 예상 수행시간

- Actual Time: 실제 수행시간
