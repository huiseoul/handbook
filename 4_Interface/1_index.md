# Intro

- Huiseoul 은 client/backend 의 data type 을 공유하기 위해 type definition repo 를 운영합니다.
- Interface를 작성할 때 고려해야 할 것들을 정리한 문서입니다.

- [type repo](https://github.com/huiseoul/type/)에 인터페이스를 생성합니다.

# How To & Consideration

### Directory 및 File 명명

- Directiory: 일반적으로 domain 별로 directory를 생성합니다.
- Filename: prefix `I` 를 붙입니다.

### 데이터 타입

- `object`, `JSON` 과 같은 general type은 가능한 사용하지 않도록 합니다.

### Optional(? mark)

- 데이터 생성 시 반드시 필요한 속성들을 필수로 합니다.
- 경우에 따라 있을 수도/없을 수도 있는 속성은 선택 옵션(`?`)을 줍니다.
  - 다른 모델과 연관 관계로 인해 추가된 속성은 대체로 선택 옵션(`?`)을 줍니다.
