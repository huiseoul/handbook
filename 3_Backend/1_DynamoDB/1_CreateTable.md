# Intro

- DynamoDB 의 table 을 생성할 때 고려해야 할 것들을 정리한 문서입니다.

# Table name

- Table name 은 `명사의 복수형`을 `PascalCase` 로 표기합니다.
  - e.g. `Carts`, `Products`

# Field name

- Field name 은 `PascalCase` 로 표기합니다.
- Boolean field 는 `Is` 라는 prefix 를 붙입니다.

# Primary Key 설정

DynamoDB 는 `partition key` 와 `sort key` 의 조합(`sort key` 는 option) 으로 item 의 `primary key`를 정의합니다.

- Huiseoul 은 `partition key` 와 `sort key` 의 조합이 해당 item 의 성격을 논리적으로 잘 드러내는 경우에만 `primary key` 로 두개의 조합을 사용합니다.
  - 예를 들어, `Carts` table 은 `UserID` 와 `ProductID` 를 각각 `partition key` 와 `sort key` 로 사용합니다. User 는 여러 개의 상품을 담을 수 있으나, 한 상품을 중복해서 담을 수는 없으므로(이 경우는 중복으로 담기는 대신 Count 가 늘어나야 합니다.) 해당 조합을 `primary key` 로 사용하는 것은 논리적입니다.
- 위의 경우를 제외한 모든 경우, `primary key` 는 `ID` 로 합니다. 이 때 `ID` 는 [`UUID`](https://en.wikipedia.org/wiki/Universally_unique_identifier) 를 사용합니다.

# Secondary indexes 설정

- DynamoDB 는 `secondary indexes` 기능을 제공합니다.
- Table 을 특정한 column 을 기준으로 나눠서 보고 싶을 경우 이를 사용합니다.
- Secondary index 는 최대 5개까지 만들 수 있습니다.
  - 기존 item 이 존재하는 테이블에 secondary index 를 추가하는 것은 시간이 걸립니다. 따라서 새로운 secondary index 를 사용하는 application 의 배포는 secondary index 생성이 완료된 후에 진행해야 합니다.
- Secondary indexes 는 복잡한 query 요구조건을 만족하기 어렵습니다.
- 보다 복잡한 query 는 다른 방식으로 해결합니다.

# Capacity 설정

DynamoDB 는 autoscaling 을 지원합니다. 특별한 문제가 없는 한 auto scaling 을 적용합니다.

# 참조문서

- https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/HowItWorks.CoreComponents.html
