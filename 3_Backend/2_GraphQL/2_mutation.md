# Type 정의

```graphql

# mutation을 구현할 때에는 input type 과 output type 을 각각 정의합니다.
input CreateContentCommentInput {
  ContentID: ID!
  Text: String!
}

type CreateContentCommentOutput {
  ContentComment: ContentComment!
}

# mutation의 `Input` 인자에 대한 타입으로 위에서 정의한 `input type`을 사용합니다.
type Mutation {
  createContentComment(
    Input: CreateContentCommentInput!
  ): CreateContentCommentOutput!
}

```

# 참고
- Github GraphQL API: https://developer.github.com/v4/explorer/
- https://medium.com/graphql-mastery/json-as-an-argument-for-graphql-mutations-and-queries-3cd06d252a04
