# 함수 실행 권한

- 모든 resolver 함수는 적절한 권한 관리를 해야 합니다
    - `context.Scope` 의 내용을 참조하여 적절한 권한을 가진 토큰이 아닌 경우 에러를 던지도록 합니다. 실제 코드상에서는 이를 위해 `allowedFor`를 사용하면 됩니다
- '내 정보'를 가져올 필요가 있을 때 `context.TokenInfo` 의 `id` 를 참조합니다. 예를 들어 `UserID`가 필요한 경우 쿼리의 input으로 넘겨 받는 정보 대신 `TokenInfo` 값을 사용합니다

# DataAccessObject의 사용

- 코드의 재사용성과 가독성 향상 및 추상화 수준 통일과 역할 분리를 위해 DAO를 사용합니다
- 데이터베이스에서 데이터를 가져오는 로직은 DAO 내에 두고 resolver 함수에서는 이를 적절히 호출하여 그 반환값을 사용하도록 합니다

# DataLoader의 사용

- 관계가 있는 데이터를 참조할 때는 N+1 문제를 회피하기 위해 DataLoader를 사용합니다

# 적절한 타이핑

- resolver 함수를 구현할 때는 미리 정의되어 있는 `HSResolverFn` 타입에 맞도록 합니다
