# Introduction

함께 문제를 풀어나가려면 여러가지 사항에 대한 공유가 필요합니다. 일하는 방식, 커뮤니케이션하는 방식 등이요. 이를 문서화하려고 합니다. 함께 만들어나가요!

# Rules

- 문서는 존댓말로 작성해주세요.
- 문장은 최대한 간결히 해주세요.
- 문서가 너무 길어지면 나누고 링크를 걸어주세요.
- header 는 가장 상위의 제목(h1)부터 단계에 맞게 사용해주세요.
- 문서의 생성 목적을 이슈로 만들고, branching 후 작업을 시작해주세요.
  - [Huiseoul flow](/5_HuiseoulFlow/1_index.md) 참조
  - Handbook repository 에 한해서, assignee 는 @johnwook 으로 해주세요.
  - 문장이나 directory 구조의 일관성을 유지하기 위해서입니다.
- 우선 작성하고 봅시다!

# Reference

- [Huiseoul handbook 만들기](https://engineering.huiseoul.com/huiseoul-handbook-%EB%A7%8C%EB%93%A4%EA%B8%B0-3629c333805)
