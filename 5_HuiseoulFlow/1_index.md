# Introduction

- Issue 의 생성부터 resolve 까지의 방법을 담은 문서입니다.
- [GitLab flow](https://gitlab.com/help/workflow/gitlab_flow.md) 를 기반으로 합니다.

# Sprint meeting

- Sprint meeting 은 매 2주마다 합니다.
- [지난 sprint 대한 회고](https://gitlab.com/huiseoul/handbook/tree/master/6_MeetingNote/Sprint)를 합니다.
- 새로운 sprint 에서 진행할 일을 공유합니다.
- Sprint meeting 에서 결정한 [hsMobile board](https://trello.com/b/54juY4KA/hsmobile) 의 카드로부터 feature issue 를 생성합니다.

# Issue 의 생성

- 모든 작업의 시작은 issue 의 생성입니다.
- issue 는 다음의 내용을 포함합니다.
  - Desired state
  - Description
  - Reference / Link (있을 경우)
    - [Description template](https://gitlab.com/help/user/project/description_templates.md) 으로 만들어도 좋을 듯 한데요. [group level 지원](https://gitlab.com/gitlab-org/gitlab-ce/issues/13729)이 될 때 하는 것이 좋을 것 같아 개요만 남겨둡니다. 
- Feature issue 의 경우 해당하는 sprint milestone 을 설정합니다.

# Issue 의 배정

- Issue 의 assignee 를 정하는 것을 배정이라고 합니다.
- Due date 을 설정합니다.
- [Time tracking](https://docs.gitlab.com/ee/workflow/time_tracking.html) 을 기록합니다.
  - Optional 입니다. 기능을 사용해보려고 해요.
- 작업 진행 상황 공유를 위해 [group 전체 issue board](https://gitlab.com/groups/huiseoul/-/boards?=) 에 `Doing` label 을 추가합니다.
- 특정한 시간에 `Doing` 으로 참여하고 있는 이슈는 가능한 하나로 제한합니다.

# MR 의 생성

- MR 은 issue 로부터 생성합니다.
- MR 의 작업 단위는 가능한 하루를 넘지 않게 계획합니다.
  - 하나의 issue 는 여러개의 MR 을 가질 수 있습니다.
- MR 은 가장 최근의 master branch 에서 branching 하면서 시작합니다.
  - 만약 다른 branch 에 강한 dependency 가 있을 경우, 해당 branch 를 기준으로 합니다.
- [Time tracking](https://docs.gitlab.com/ee/workflow/time_tracking.html) 을 기록합니다.
  - Optional 입니다. 기능을 사용해보려고 해요.

# MR 의 공유

- 작업중인 MR 은 issue 말머리에 WIP 를 표시합니다.
- 작업중인 MR 의 리뷰가 필요할 경우 mention 을 통해서 요청합니다.
- 작업을 시작한 후 세 시간이 넘었다면, 완료되지 않았더라도 그때까지 한 작업을 WIP 상태로 공유합니다.

# MR 의 수정

- 한번 remote 로 push 한 commit 은 rebase 하거나 변경하지 않습니다.
- 리뷰 및 MR 의 수정은 MR 의 작성자 뿐 아니라 리뷰어를 포함한 모든 팀원이 가능합니다.

# MR 의 merge

- 완료된 MR 은 팀원 중 한명에게 assign 합니다.
- 이 때 assign 은 임의로 합니다. 가능한 골고루 맡을 수 있도록 해주세요.
- 해당 MR 의 완료로 issue 작업이 마무리 되었다면, issue 를 `Doing` 에서 `In Review` 로 [이동해주세요.](https://gitlab.com/groups/huiseoul/-/boards?=)
  - MR 이 merge 되면 해당 issue 를 close 할 수 있도록 해주세요.(ex. [automatic closing](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html))
- MR 을 넘겨받은 팀원은 이 MR 의 리뷰부터 merge 까지를 담당합니다.
  - 리뷰가 필요할 경우 다른 팀원들에게 리뷰를 요청합니다.
  - 수정이 필요할 경우 작성자에게 수정을 요청하거나 직접 수정합니다.
  - Assignee 를 포함해서 둘 이상의 팀원의 동의를 받으면, merge 합니다.
- MR 을 merge 할 때 특별한 이유(e.g. 다른 branch 로의 merge 가 필요)가 없다면, source branch 를 항상 삭제합니다.

# Branching

- Continuous delivery 가 불가능한 project (e.g. hsMobile, admin, CRM) 의 경우 production branch 를 운영합니다.
- Production branch 로의 MR 은 실제 배포가 일어날 때 합니다. 

# 참조

- [GitLab flow](https://gitlab.com/help/workflow/gitlab_flow.md) 
